<?php

namespace Drupal\minimal_translation_revision;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MinimalTranslationRevision.
 *
 * @package Drupal\minimal_translation_revision
 */
class MinimalTranslationRevision implements ContainerInjectionInterface {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  /**
   * Processing content entity object.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;
  /**
   * Entity type object.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;
  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * MinimalTranslationRevision constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager.
   */
  public function __construct(
    Connection $database,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    $this->database           = $database;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Set processing content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity object.
   *
   * @return \Drupal\minimal_translation_revision\MinimalTranslationRevision
   *   Fluent method.
   */
  public function setEntity(ContentEntityInterface $entity) {
    $this->entity     = $entity;
    $this->entityType = $this->entity->getEntityType();
    return $this;
  }

  /**
   * Remove all redundant revision data(for previous revisions only).
   */
  public function wipe() {
    // Prevent processing non-translatable or non-revisionable entities.
    if ($this->needsProcessing()) {
      // Remove all non-affected field revisions.
      $this->wipeFieldsRevisionTables();
      // Remove all non-affected revisions
      // from the entity's revision data table.
      $this->database
        ->delete($this->entityType->getRevisionDataTable())
        // Exclude latest revision.
        ->condition($this->entityType->getKey('revision'), $this->entity->getRevisionId(), '<')
        // Prevent processing other entities.
        ->condition($this->entityType->getKey('id'), $this->entity->id())
        // Process only non-affected revisions.
        ->isNull($this->entityType->getKey('revision_translation_affected'))
        ->execute();
    }
  }

  /**
   * Wipe fields revision tables.
   */
  protected function wipeFieldsRevisionTables() {
    // Select non-affected revisions data.
    $non_affected_revisions = $this->fetchNonAffectedRevisionsData();
    if (!empty($non_affected_revisions)) {
      $definitions = $this->entityFieldManager
        ->getFieldDefinitions($this->entityType->id(), $this->entity->bundle());
      foreach ($definitions as $field_name => $definition) {
        if (!$definition->isTranslatable()
          || !$this->entity->hasField($field_name)
          || !$this->database->schema()
            ->tableExists($this->getFieldRevisionTable($field_name))) {
          continue;
        }
        // Wipe all non-affected revisions from the revision fields tables.
        foreach ($non_affected_revisions as $data) {
          $this->database
            ->delete($this->getFieldRevisionTable($field_name))
            ->condition('bundle', $this->entity->bundle())
            ->condition('entity_id', $this->entity->id())
            ->condition('revision_id', $data[$this->entityType->getKey('revision')])
            ->condition('langcode', $data[$this->entityType->getKey('langcode')])
            ->execute();
        }
      }
    }
  }

  /**
   * Fetch non-affected revisions data.
   *
   * @return array
   *   Non-affected revisions data.
   */
  protected function fetchNonAffectedRevisionsData() {
    $select = $this->database->select($this->entityType->getRevisionDataTable(), 'r');
    $select->fields('r', [$this->entityType->getKey('revision'), $this->entityType->getKey('langcode')]);
    $select->condition($this->entityType->getKey('id'), $this->entity->id());
    $select->isNull($this->entityType->getKey('revision_translation_affected'));
    return (array) $select->execute()->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Get field revision table name.
   *
   * @param string $field_name
   *   Field name.
   *
   * @return string
   *   Field revision table name.
   */
  private function getFieldRevisionTable($field_name) {
    return (string) $this->entityType->getRevisionTable() . '__' . $field_name;
  }

  /**
   * Check whether the entity type is translatable and revisionable.
   *
   * @return bool
   *   Checking result.
   */
  private function needsProcessing() {
    return $this->entityType->isTranslatable()
      && $this->entityType->isRevisionable();
  }

}
